<?php
/**
 * Curse Inc.
 * Hydralytics
 * Site Stats Hourly Job
 *
 * @author		Brent Copeland
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Hydralytics
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace Hydralytics;

class SiteStatsHourlyJob extends \SyncService\Job {
	/**
	 * Job to update the hourly analytics table.
	 *
	 * @access	public
	 * @param	array	[Unused] Job arguments.
	 * @return	integer Exit value for this process.
	 */
	public function execute($args = []) {
		//@TODO: Likely broken when forking.
		$db = wfGetDB(DB_MASTER);

		$values = [];
		$testDate = '01';
		$todaysDate = date('mdY');
		$todaysTimestamp = gmmktime(0, 0, 0);
		$hour = date('G');

		//Get Statistics
		$this->outputLine("Refresh Site Statistics\n\n");
		$counter = new \SiteStatsInit();

		$this->outputLine("Counting total edits...");
		$edits = $counter->edits();
		$this->outputLine("{$edits}\n");

		$this->outputLine("Today's Timestamp: {$todaysTimestamp}\n");

		$result = $db->select(
			'analytics_hourly_edits',
			['aheid', 'date'],
			'aheid > 0',
			__METHOD__,
			[
				'ORDER BY'	=> 'aheid DESC',
				'LIMIT'		=> '1'
			]
		);
		if ($result) {
			$todayExists = $result->fetchRow();
			$testDate = date('mdY', $todayExists['date']);
		}

		if ($testDate == $todaysDate) {
			$this->outputLine("Found existing row with hourly entries for today.  Updating this hour_{$hour} only...\n");

			$success = $db->update(
				'analytics_hourly_edits',
				[
					'hour_'.$hour => $edits
				],
				['date' => $todayExists['date']],
				__METHOD__
			);
			if ($success) {
				$this->outputLine("Added {$edits} edits to hour_{$hour} for {$todayExists['date']}.\n");
				return 0;
			} else {
				$this->outputLine("ERROR: Could not add edits to hour_{$hour} statistics for {$todayExists['date']}.\n");
				return 1;
			}
		} else {
			$this->outputLine("Inserting hour_{$hour} on new row for {$todaysTimestamp}...\n");

			$success = $db->insert(
				'analytics_hourly_edits',
				[
					'hour_'.$hour	=> $edits,
					'date'			=> $todaysTimestamp
				],
				__METHOD__
			);
			if ($success) {
				$this->outputLine("Added new row of hourly edits for {$todaysTimestamp}.\n");
				return 0;
			} else {
				$this->outputLine("ERROR: Could not add hourly edits for {$todaysTimestamp}.\n");
				return 1;
			}
		}
		return 1;
	}

	/**
	 * Return cron schedule if applicable.
	 * Every hour on the minute.
	 *
	 * @access	public
	 * @return	mixed	False for no schedule or an array of schedule information.
	 */
	static public function getSchedule() {
		return [
			[
				'minutes' => '0',
				'hours' => '*',
				'days' => '*',
				'months' => '*',
				'weekdays' => '*',
				'arguments' => []
			]
		];
	}
}