<?php
/**
 * Curse Inc.
 * Hydralytics
 * Information Class
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2018 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Hydralytics
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace Hydralytics;

use Cheevos\Cheevos;
use User;

class Information {
	/**
	 * Google Analytics
	 *
	 * @var		object
	 */
	static private $ga = null;

	/**
	 * Property Being Used for GA
	 *
	 * @var 	string
	 */
	static private $property = null;

	/**
	 * Get a GA instance.
	 *
	 * @access	private
	 * @return	object	GoogleAnalytics
	 */
	static private function getGoogleAnalytics() {
		global $dsSiteKey;

		if (self::$ga === null) {
			//We can not rely on this in the future to get the property ID when on a child wiki, but this is a for now solution.
			$properties = \HydraHooks::getAdBySlot('googleanalyticsid');
			if ($properties !== false) {
				$property = self::extractGAProperty($properties);
				self::$property = $property;
				self::$ga = new GoogleAnalytics();
				self::$ga->loadGAInfo([$dsSiteKey => $property]);
			} else {
				throw new \MWException('No Google Analytics property IDs set for this wiki.');
			}
		}
		return self::$ga;
	}

	/**
	 * Extract a single Google Analytics property that should be the one for this wiki from a list.
	 *
	 * @access	public
	 * @param	string	Newline separated property IDs.
	 * @return	string	Google Analytics Property ID
	 */
	static public function extractGAProperty($properties) {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$stripGAProperties = $config->get('StripGAProperties');
		foreach ($stripGAProperties as $property) {
			$properties = str_replace($property, '', $properties);
		}
		$properties = trim($properties);
		list($property) = explode("\n", $properties);
		return $property;
	}

	/**
	 * Return the property we have set
	 *
	 * @return string
	 */
	static public function getProperty() {
		self::getGoogleAnalytics();
		return self::$property;
	}

	/**
	 * Get the wiki managers on this wiki.
	 *
	 * @access	public
	 * @return	array	Wiki Managers
	 */
	static public function getWikiManagers() {
		global $wgWikiManagers;
		return (array) $wgWikiManagers;
	}

	/**
	 * Get a link to the FAQ page.
	 *
	 * @access	public
	 * @return	string	HTML Link
	 */
	static public function getFaqLink() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		return \Linker::makeExternalLink(
			$config->get('HLFaqUrl'),
			wfMessage('hlfaqurl-text')
		);
	}

	/**
	 * Get a link to the Feedback page.
	 *
	 * @access	public
	 * @return	string	HTML Link
	 */
	static public function getFeedbackLink() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		return \Linker::makeExternalLink(
			$config->get('HLFeedbackUrl'),
			wfMessage('hlfeedbackurl-text')
		);
	}

	/**
	 * Get a link to the Discord page.
	 *
	 * @access	public
	 * @return	string	HTML Link
	 */
	static public function getDiscordLink() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		return \Linker::makeExternalLink(
			$config->get('HLDiscordUrl'),
			wfMessage('hldiscordurl-text')
		);
	}

	/**
	 * Get the number of edit per day for this wiki over the past X days.
	 *
	 * @access	public
	 * @param	integer	[Optional] Number of days in the past to retrieve.
	 * @return	array	[day timestamp => points]
	 */
	static public function getEditsPerDay($days = 30) {
		global $dsSiteKey;

		$filters = [
			'site_key'	=> $dsSiteKey,
			'stat'		=> 'article_edit',
			'limit'		=> $days,
			'offset'	=> 0
		];

		$edits = [];
		$statProgress = [];
		try {
			$statProgress = \Cheevos\Cheevos::getStatDailyCount($filters);
		} catch (\Cheevos\CheevosException $e) {
			wfDebug(__METHOD__." - Could not contact Cheevos due to: ".$e->getMessage());
		}

		for ($i = 0; $i < $days; $i++) {
			$edits[strtotime('Today - '.$i.' days')] = 0;
		}

		foreach ($statProgress as $progress) {
			if (isset($edits[$progress->getDay()])) {
				$edits[$progress->getDay()] = $progress->getCount();
			}
		}

		return $edits;
	}

	/**
	 * Get the top editors for the wiki over all time or optionally with monthly counts.
	 *
	 * @access	public
	 * @param	boolean	[Optional] Group by month.
	 * @return	array	[['points' => 0, 'month' => null for global or month display, 'user' => User object]]
	 */
	static public function getTopEditors($isMonthly = false) {
		global $dsSiteKey;

		$editors = [];
		$statProgress = \Cheevos\Points\PointsDisplay::getPoints($dsSiteKey, null, 25, 0, true, $isMonthly);

		$userCount = 0;
		foreach ($statProgress as $progress) {
			$user = Cheevos::getUserForServiceUserId($progress->getUser_Id());
			if ($user && User::isCreatableName($user->getName()) && !$user->isHidden()) {
				$userCount++;
				$editors[] = [
					'points'	=> $progress->getCount(),
					'month'		=> ($isMonthly ? $progress->getMonth() : null),
					'user'		=> $user
				];
				if ($userCount >= 10) {
					break; // we have 10 valid lookups.
				}
			}
		}

		return $editors;
	}

	/**
	 * Get the percentage of edits per day, logged in and out, for this wiki over the past X days.
	 *
	 * @access	public
	 * @param	integer	[Optional] Number of days in the past to retrieve.
	 * @return	array	[day timestamp => points]
	 */
	static public function getEditsLoggedInOut($days = 30) {
		global $dsSiteKey;

		$filters = [
			'site_key'	=> $dsSiteKey,
			'stat'		=> 'article_edit_is_logged_in',
			'limit'		=> $days,
			'offset'	=> 0
		];

		$editsLoggedIn = [];
		$editsLoggedOut = [];
		try {
			$statProgress = \Cheevos\Cheevos::getStatDailyCount($filters);

			for ($i = 0; $i < $days; $i++) {
				$editsLoggedIn[strtotime('Today - '.$i.' days')] = 0;
				$editsLoggedOut[strtotime('Today - '.$i.' days')] = 0;
			}

			foreach ($statProgress as $progress) {
				if (isset($editsLoggedIn[$progress->getDay()])) {
					$editsLoggedIn[$progress->getDay()] = $progress->getCount();
				}
			}

			$filters['stat'] = 'article_edit_is_logged_out';

			$statProgress = \Cheevos\Cheevos::getStatDailyCount($filters);

			foreach ($statProgress as $progress) {
				if (isset($editsLoggedOut[$progress->getDay()])) {
					$editsLoggedOut[$progress->getDay()] = $progress->getCount();
				}
			}
		} catch (\Cheevos\CheevosException $e) {
			wfDebug(__METHOD__." - Could not contact Cheevos due to: ".$e->getMessage());
		}

		return ['in' => $editsLoggedIn, 'out' => $editsLoggedOut];
	}

	/**
	 * Return the top searchs by rank for the given time period.
	 *
	 * @access	public
	 * @param	integer	[Optional] Start Timestamp, Unix Style
	 * @param	integer	[Optional] End Timestamp, Unix Style
	 * @return	array	Top search terms by rank in descending order.  [[rank, term], [rank, term]]
	 */
	static public function getTopSearchTerms($startTimestamp = null, $endTimestamp = null) {
		return \SearchLoggerHooks::getTopSearchTerms($startTimestamp, $endTimestamp, 10);
	}

	/**
	 * Return geolocation data.
	 *
	 * @access	public
	 * @param	integer	[Optional] Start Timestamp, Unix Style
	 * @param	integer	[Optional] End Timestamp, Unix Style
	 * @return	array	Geolocation by: [percentage => Country Code]
	 */
	static public function getGeolocation($startTimestamp = null, $endTimestamp = null) {
		global $dsSiteKey;

		self::getGoogleAnalytics();
		$data = self::$ga->getGeographicData($startTimestamp, $endTimestamp, $dsSiteKey);
		$data = self::stripGAPrefix($data);
		return $data;
	}

	/**
	 * Return the top view pages for this wiki.
	 *
	 * @access	public
	 * @param	integer	[Optional] Start Timestamp, Unix Style
	 * @param	integer	[Optional] End Timestamp, Unix Style
	 * @return	array	Top Viewed Pages
	 */
	static public function getTopViewedPages($startTimestamp = null, $endTimestamp = null) {
		global $dsSiteKey;

		self::getGoogleAnalytics();
		$data = self::$ga->getPopularPages($startTimestamp, $endTimestamp, $dsSiteKey, 20);
		$data = self::stripGAPrefix($data);
		return $data;
	}

	/**
	 * Return the top view file for this wiki.
	 *
	 * @access	public
	 * @param	integer	[Optional] Start Timestamp, Unix Style
	 * @param	integer	[Optional] End Timestamp, Unix Style
	 * @return	array	Top Viewed Files
	 */
	static public function getTopViewedFiles($startTimestamp = null, $endTimestamp = null) {
		global $dsSiteKey;

		self::getGoogleAnalytics();
		$data = self::$ga->getPopularFiles($startTimestamp, $endTimestamp, $dsSiteKey);
		$data = self::stripGAPrefix($data);
		return $data;
	}

	/**
	 * Return daily sessions and page views.
	 *
	 * @access	public
	 * @param	integer	[Optional] Start Timestamp, Unix Style
	 * @param	integer	[Optional] End Timestamp, Unix Style
	 * @return	array	Daily sessions and page views.
	 */
	static public function getDailyTotals($startTimestamp = null, $endTimestamp = null) {
		global $dsSiteKey;

		self::getGoogleAnalytics();
		$_data = self::$ga->getDailyData($startTimestamp, $endTimestamp, $dsSiteKey);

		$data = [];
		if (isset($_data['data'])) {
			$data = $_data['data'];
		}

		$data = self::stripGAPrefix($data);
		return $data;
	}

	/**
	 * Return cumulative thirty day sessions and page views.
	 *
	 * @access	public
	 * @param	integer	[Optional] Start Timestamp, Unix Style
	 * @param	integer	[Optional] End Timestamp, Unix Style
	 * @return	array	Cumulative thirty day sessions and page views.
	 */
	static public function getMonthlyTotals($startTimestamp = null, $endTimestamp = null) {
		global $dsSiteKey;

		self::getGoogleAnalytics();
		$_data = self::$ga->getTotalsData($startTimestamp, $endTimestamp, $dsSiteKey);

		$data = [];
		if (isset($_data['data'])) {
			$data = $_data['data'];
		}

		$data = self::stripGAPrefix($data);
		return $data;
	}

	/**
	 * Return device break down information.
	 *
	 * @access	public
	 * @param	integer	[Optional] Start Timestamp, Unix Style
	 * @param	integer	[Optional] End Timestamp, Unix Style
	 * @return	array	Device Breakdown
	 */
	static public function getDeviceBreakdown($startTimestamp = null, $endTimestamp = null) {
		global $dsSiteKey;

		self::getGoogleAnalytics();
		$data = self::$ga->getDeviceBreakdown($startTimestamp, $endTimestamp, $dsSiteKey);
		$data = self::stripGAPrefix($data);
		return $data;
	}

	/**
	 * Strip 'ga:' off the beginning of keys in an array.
	 *
	 * @access	public
	 * @param	array	Array to clean keys of.
	 * @return	array	Array keys stripped of 'ga:'.
	 */
	static private function stripGAPrefix($_data) {
		if (!is_array($_data)) {
			return $_data;
		}
		$data = [];
		foreach ($_data as $key => $value) {
			$data[str_replace('ga:', '', $key)] = $value;
		}
		return $data;
	}
}
